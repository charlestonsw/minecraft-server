<?php

require_once( MCSERVER_HAPI_DIR . 'hoverboard.object.php' );

/**
 * Class MCServer
 *
 * Text Domain: csamcserver
 */
class MCServer extends Hoverboard_Object {

    /**
     * @var MCServer
     **/
    private static $instance = null;

    /**
     * @var MCServer_Admin
     */
    public $admin = null;

    /**
     * Singleton
     * @static
     */
    public static function init() {
        if ( is_null( self::$instance ) ) {

            if ( did_action( 'plugins_loaded' ) ) {
                self::plugin_textdomain();
            } else {
                add_action('plugins_loaded', array(__CLASS__, 'plugin_textdomain'), 99);
            }

            self::$instance = new MCServer;
        }

        return self::$instance;
    }

    /**
     * Load language files
     */
    public static function plugin_textdomain() {
        load_plugin_textdomain( 'csamcserver', false, dirname( plugin_basename( MCSERVER__PLUGIN_FILE ) ) . '/languages/' );
    }


}
