<?php
require_once( MCSERVER_HAPI_DIR . 'hoverboard.object.php' );

/**
 * Class MCServer_Admin_Settings
 *
 * Text Domain: csamcserver
 */
class MCServer_Admin_Settings extends Hoverboard_Object {

    /**
     * Render the admin settings page.
     */
    public function render() { ?>
        <div class="wrap">
            <div class="icon32" id="icon-options-general"><br /></div>
            <h2><?php esc_html_e( 'Minecraft Server Settings', 'csamcserver' ); ?></h2>
            <?php $this->render_content(); ?>
        </div>
    <?php
    }

    /**
     * Render the main content area.
     */
    private function render_content() {
        echo 'hi';
    }

}

