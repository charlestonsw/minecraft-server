<?php
require_once( MCSERVER_HAPI_DIR . 'hoverboard.admin.php' );

/**
 * Class MCServer_Admin
 *
 * Text Domain: csamcserver
 */
class MCServer_Admin extends Hoverboard_Admin {

    /**
     * @var MCServer
     */
    private $mcserver;

    /**
     * @var MCServer_Admin
     **/
    private static $instance = null;

    /**
     * @var MCServer_Admin_Settings
     */
    private $settings = null;

    /**
     * Create a singleton of this admin object.
     *
     * @return MCServer_Admin
     */
    static function init() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new MCServer_Admin;
        }
        return self::$instance;
    }

    /**
     * Create the MCServer_Admin object.
     */
    function __construct() {
        parent::__construct();
        $this->mcserver = MCServer::init();
        $this->mcserver->admin = self::$instance;
        $this->add_hooks_and_filters();
    }

    /**
     * Setup the WordPress hooks and filters.
     */
    private function add_hooks_and_filters() {
        add_action( 'admin_menu' , array( $this , 'attach_settings_submenu') );
    }

    /**
     * Add new entries to the WordPress settings menu.
     */
    function attach_settings_submenu() {
        add_submenu_page(
            'options-general.php'                               , // parent slug
            __('Minecraft Server Settings'  , 'csamcserver')    , // page title
            __('MC Server'                  , 'csamcserver')    , // menu title
            'activate_plugins'                                  , // capability
            'mcserver'                                          , // menu slug
            array( $this, 'render_mcserver_settings' )           // render method
        );
    }

    /**
     * Create and attach the admin settings object.
     */
    private function create_object_MCServer_Admin_Settings() {
        if ( is_null( $this->settings ) ) {
            require_once( MCSERVER__PLUGIN_DIR . 'include/class.admin.settings.php' );
            $this->settings = new MCServer_Admin_Settings;
        }
    }

    /**
     * Render the Settings / Minecraft Server page.
     */
    function render_mcserver_settings() {
        $this->create_object_MCServer_Admin_Settings();
        $this->settings->render();
    }

}

MCServer_Admin::init();
