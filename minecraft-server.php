<?php
/*
 * Plugin Name: Minecraft Server
 * Plugin URI: http://www.charlestonsw.com/product/minecraft-server/
 * Description: Install and manage a Minecraft PC server from your WordPress site.
 * Author: Charleston Software Associates
 * Version: 4.2.00
 * Author URI: http://www.charlestonsw.com
 * License: GPL2+
 * Text Domain: csamcserver
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly, dang hackers

define( 'MCSERVER_VERSION'      ,   '4.2.00'                    );
define( 'MCSERVER__PLUGIN_DIR'  ,   plugin_dir_path( __FILE__ ) );
define( 'MCSERVER__PLUGIN_FILE' ,   __FILE__                    );
define( 'MCSERVER_HAPI_DIR'     , MCSERVER__PLUGIN_DIR . 'hapi/'    );

// The Base plugin
//
require_once( MCSERVER__PLUGIN_DIR . 'include/class.mcserver.php'   );

// Admin Features
//
if ( is_admin() ) {
    require_once( MCSERVER__PLUGIN_DIR . 'include/class.admin.php'  );
}

add_action( 'init', array( 'MCServer', 'init' ) );

// Dad. Explorer. Rum Lover. Code Geek.  Not Necessarily In That Order. //