=== Minecraft Server ===
Plugin Name:  Minecraft Server
Contributors: charlestonsw
Donate link: http://www.charlestonsw.com/product/minecraft-server/
Tags: minecraft, minecraft server, self-hosted, gaming
Requires at least: 3.8
Tested up to: 4.2.2
Stable tag: 4.2.00
License: GLPv2 or later

Install and manage a Minecraft PC server from your WordPress site.

== Description ==

 Downloads and installs the latest Minecraft server application and provides a WordPress admin interface to manage the Minecraft server.

 A [Hoverboard](http://hoverboard.tools/) compatible plugin.

== Installation ==

= Requirements =

* Linux OS
* [Java Runtime Engine](http://java.com/en/download/installed.jsp)
* WordPress: 3.8
* PHP 5.2.4 (same as WordPress 3.2+)
* MySQL 5.0.15 (same as WordPress 3.2+)

== FAQ ==

= Which Minecraft Server is installed? =

This plugin will fetch the Minecraft PC Server.  As of this writing version 1.8.6 will be retrieved.

= Where do I go to change the settings? =

Login as a WordPress site admin and look under the Settings menu for "MC Server".  Don't see it?  Make sure you have the activate_plugins capability for your admin user.

= What is Hoverboard? =

[Hoverboard](http://hoverboard.tools/) is a plugin developer toolkit and premium plugin storefront system that helps plugin developers, like me, monetize WordPress plugins.
Monetizing plugins is a good thing.
The premium version of my plugin provides my customers with cool added features on top of all the neat stuff my free plugin provides while giving you an easy and cost-effective way to support my work.
If I can make even a small bit of income from my plugins it allows me to allocate time to the project that I would otherwise need to spend working on projects for someone else.


== Changelog ==

= 4.2.00 =

* Initial release.